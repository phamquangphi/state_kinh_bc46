import React from "react";
import "./style.scss";
import ItemDoiKinh from "./ItemDoiKinh";
const ListDoiKinh = ({ data, handlePrList }) => {
  return (
    <footer>
      <div className="mt-5 footBg ">
        <h2 className="text-center">Mẫu Kính Bạn Chọn</h2>
        <div className="row mt-3">
          {data.map((item) => {
            // console.log(item);
            return (
              <ItemDoiKinh
                handlePrList={handlePrList}
                pr={item}
                key={item.id}
              />
            );
          })}
        </div>
      </div>
    </footer>
  );
};

export default ListDoiKinh;
