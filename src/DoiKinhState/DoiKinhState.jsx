import React, { useState } from "react";
import ListDoiKinh from "./ListDoiKinh";
import data from "./data.json";
const DoiKinhState = () => {
  const [prList, setPrList] = useState(data[0]);
  const handlePrList = (pr) => {
    setPrList(pr);
  };
  return (
    <div>
      {/**Phần đầu của trang hình mẫu và thông tin kính */}
      <div className="ListDoiKinh">
        <nav className="navBg">
          <h1>Try glass app online</h1>
        </nav>
        <header className="mt-5">
          <div className="imgMau">
            <img src="./glassesImage/model.jpg" alt="" />
          </div>
          <div className="imgKinh">
            <img src={prList.url} alt="..." />
          </div>
          <div className="titleKinh">
            <h2>{prList.name}</h2>
            <p>{prList.desc}</p>
            <span>{prList.price} $</span>
          </div>
        </header>
        <ListDoiKinh data={data} handlePrList={handlePrList} />
      </div>
    </div>
  );
};

export default DoiKinhState;
